.PHONY: init
init:
	docker-compose up -d postgres
	docker-compose run --rm wait_postgres
	docker-compose run --rm db_migrate -dir /migrations up

.PHONY: tests
tests:
	docker-compose run --rm db_tests

.PHONY: goose
goose:
	docker-compose run --rm db_migrate -dir /migrations

.PHONY: destroy
destroy:
	docker-compose stop
	docker-compose rm -f
	rm -rf data
