# Unit testing a PostgreSQL database schema with pgTAP

## Database testing

```shell
make init
make tests
make destroy
```

## Database migrations

```shell
docker compose run --rm db_migrate -dir /migrations status
docker compose run --rm db_migrate -dir /migrations up
docker compose run --rm db_migrate -dir /migrations down
docker compose run --rm db_migrate -dir /migrations fix
```
