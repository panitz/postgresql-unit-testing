-- +goose Up
-- +goose StatementBegin
CREATE SEQUENCE world.seq_countries;

CREATE TABLE world.countries
(
    id INTEGER NOT NULL DEFAULT nextval(('world.seq_countries'::text)::regclass),
    name VARCHAR(100) NOT NULL,

    CONSTRAINT pk_countries PRIMARY KEY (id)
);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE world.countries;
DROP SEQUENCE world.seq_countries;
-- +goose StatementEnd
