-- +goose Up
-- +goose StatementBegin
CREATE SCHEMA world;
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP SCHEMA world;
-- +goose StatementEnd
