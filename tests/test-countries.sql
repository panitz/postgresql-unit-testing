BEGIN;
    SELECT plan(6);
    SELECT has_sequence('world', 'seq_countries', 'Sequence "world.seq_countries" should exist.');
    SELECT has_table('world', 'countries', 'Table "world.countries" should exist.');
    SELECT columns_are('world', 'countries', ARRAY['id', 'name']);
    SELECT col_type_is('world', 'countries', 'id', 'integer', 'world.countries.id should be INTEGER');
    SELECT col_is_pk('world', 'countries', 'id', 'world.countries.id should be PRIMARY KEY');
    SELECT col_type_is('world', 'countries', 'name', 'character varying(100)', 'world.countries.name should be VARCHAR(100)');
ROLLBACK;
